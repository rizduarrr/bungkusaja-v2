from django import forms  
from webReview.models import Data

class DataForm(forms.ModelForm):  
    class Meta:  
        model = Data 
        fields = "__all__"
