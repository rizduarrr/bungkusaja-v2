# Create your tests here.
from django.http import HttpRequest
from django.test import TestCase
from django.test import SimpleTestCase
from django.urls import reverse

from . import views
from .models import *
from .forms import *

# harus make testcase biasa karena di feedback formsnya ditampilin
class UnitTests(TestCase):

    # test ada atau nggak by name dan apakah templatenya sesuai
    '''
    def test_view_uses_correct_template(self):
        response = self.client.get('/submit/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'submit.html')
    '''