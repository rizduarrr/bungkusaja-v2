from django.urls import path
from . import views
from webReview import views

app_name = 'webReview'

urlpatterns = [
    # Untuk forms
    path('submit/', views.emp, name='submit'),  
    path('',views.show, name='ulasan'),    
    path('delete/<int:id>', views.destroy),
    
]
