from django.shortcuts import render, redirect  
from feedback.forms import DataForm  
from feedback.models import Data

# TestCase
from django.views.generic import TemplateView

# Create your views here.  
def emp(request):  
    if request.method == "POST":  
        form = DataForm(request.POST)  
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('/ulasan/')  
            except:  
                pass  
    else:  
        form = DataForm()  
    return render(request,'submit.html',{'form':form}) 

def show(request):  
    data = Data.objects.all()  
    return render(request,"ulasan.html",{'data':data})  

def destroy(request, id):  
    data = Responses.objects.get(id=id)  
    data.delete()  
    return redirect("/submit/")  

# For TestCase

class SubmitPageView(TemplateView):
    template_name = 'submit.html'


class ReviewsPageView(TemplateView):
    template_name = 'ulasan.html'
