from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Data(models.Model):   
    user_name = models.CharField(max_length=20)
    user_email = models.EmailField()
    user_review = models.CharField(max_length=160)
    user_rating = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    class Meta:  
        db_table = "data" 
