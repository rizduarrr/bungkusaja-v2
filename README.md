[![pipeline status](https://gitlab.com/rizduarrr/bungkusaja-v2/badges/master/pipeline.svg)](https://gitlab.com/rizduarrr/bungkusaja-v2/commits/master)

[![coverage report](https://gitlab.com/rizduarrr/bungkusaja-v2/badges/master/coverage.svg)](https://gitlab.com/rizduarrr/bungkusaja-v2/commits/master)


Bungkusaja-v2

Deskripsi Pembagian Kerja

Ariq Rahmatullah        :  Menciptakan bagian USER memesan makanan

Muhammad Zakiy Saputra  :  Membuat bagian USER memberikan review terhadap katering yang sudah dipesan, membuat base.html, dan user authentication

Nabila Sekar Andini     :  Membuat fitur login dan logout dalam aplikasi accounts menggunakan models termodifikasi pada django serta authorisation user.

Nunun Hidayah           :  Membuat bagian USER memberikan review terhadap website.


>  Notes :
>  Kak Rizdar, aku udah bikin workable navbar + user authentication tapi temenku lupa ngepull sebelum ngepush jadi error (+ ilang authenticationnya karena aku baru nyadar pas ngecek hari ini) navbarnya.
>  Kalau aku boleh request untuk consider nilai berdasarkan push sebelumnya, mohon liat commit yang ini :"
>  Bukti : https://gitlab.com/rizduarrr/bungkusaja-v2/commit/a1c2ece8712d0cbd83324a6d46f475bd61404ff0
>  -zakiy

