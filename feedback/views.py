from django.shortcuts import render, redirect  
from feedback.forms import ResponsesForm  
from feedback.models import Responses

from django.core import serializers
from django.http import HttpResponse

import datetime

# TestCase
from django.views.generic import TemplateView

# Create your views here.  
def getData(request):
    Responses = Responses.objects.all()
    Responses_list = serializers.serialize('json', Responses)
    return HttpResponse(Responses_list, content_type="text/json-comment-filtered")

def emp(request):
    form = ResponsesForm()
    if request.method == "POST":  
        form = ResponsesForm(request.POST)  
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('/feedback/review/')  
            except:  
                pass  
    else:  
        form = ResponsesForm()  
    return render(request,'review.html',{'form':form})  

def show(request):  
    responses = Responses.objects.all()  
    return render(request,"feedback.html",{'responses':responses})   

# For TestCase

class ReviewPageView(TemplateView):
    template_name = 'review.html'


class FeedbackPageView(TemplateView):
    template_name = 'feedback.html'
