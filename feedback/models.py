from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Responses(models.Model):  
    
    ename = models.CharField(max_length=20)
    emitra = models.CharField(max_length=20)
    ereview = models.CharField(max_length=160)
    erating = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    class Meta:  
        db_table = "responses" 
