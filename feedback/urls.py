from django.urls import path
from . import views
from feedback import views

app_name = 'feedback'

urlpatterns = [
    # Untuk forms
    path('review/', views.emp, name='review'),  
    path('',views.show, name='feedback'),
]