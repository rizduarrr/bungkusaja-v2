from django import forms  
from feedback.models import Responses
class ResponsesForm(forms.ModelForm):  
    class Meta:  
        model = Responses 
        fields = "__all__"
