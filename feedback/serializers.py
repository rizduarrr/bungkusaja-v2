# serializers.py
from rest_framework import serializers
from feedback.models import Responses


class ResponsesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Responses
        fields = ['id', 'ename', 'emitra', 'ereview', 'erating']