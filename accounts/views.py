from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import JsonResponse

from .forms import SignUpForm #, SignInForm

# Create your views here.
def indexView(request): 
    return render(request, 'index.html')

def caribukuView(request):
    return render(request, 'caribuku.html')
    
def get_data(request):
    key = request.GET['key']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
    response = requests.get(url)
    response_json = response.json()

    return JsonResponse(response_json)

@login_required
def profileView(request):
    return render(request, 'profile.html')

def registerView(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts:login_url')
    else:
        form = SignUpForm()

    return render(request, 'registration/register.html', {'form':form})



# def loginView(request):
#     if request.method == 'POST':
#         form = SignInForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('accounts:profile')
    
#     else:
#         form = SignInForm()
    
#     return render(request, 'accounts/login.html', {'form':form})