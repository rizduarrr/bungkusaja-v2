from django.urls import path, include
from . import views
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib import admin
from .views import get_data

app_name = 'accounts'

urlpatterns = [
    path('', views.indexView, name='index'),
    path('profile/', views.profileView, name = 'profile' ),
    path('login/', LoginView.as_view(), name = 'login_url'), 
    path('register/', views.registerView, name = 'register_url'),
    path('logout/', LogoutView.as_view(next_page='accounts:profile'), name = 'logout'),
    path('caribuku/', views.caribukuView, name='caribuku'),
    path('getData', get_data)
]